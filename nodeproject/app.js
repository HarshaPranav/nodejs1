var express = require("express");
var app = express();

var port = 8080;
var eventRouter = require('./src/routes/eventRoutes');

app.use(express.static('public'));
app.use(express.static('bower_components'));
// app.use(express.static('src/views'));

app.set('views','./src/views');
app.set('view engine','ejs');

app.use('/Events', eventRouter);

app.get('/', function(req, res) {
    // res.send('Aloha World!');
    res.render('index', {
        list:['1st value','2nd value','3rd value'],
        nav : [ 
            {Link: 'Services', Text: 'Services'},
            {Link: 'Portfolio', Text: 'Portfolio'},
            {Link: 'About', Text : 'About'},
            {Link: 'Team', Text : 'Team'},
            {Link: 'Contact', Text : 'Contact'},
            {Link: 'Events', Text : 'Events'}
        ]
    });
});


app.get('/routing', function(req, res) {
    res.send('Aloha Routing!');
    
});

app.listen(port, function(err){
   console.log('The server is running on port:' + port); 
});