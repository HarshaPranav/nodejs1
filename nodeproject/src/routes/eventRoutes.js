var express = require('express');

var eventRouter = express.Router();

eventRouter.route('/')
    .get(function(req,res){
    res.render('events', {
        list:['1st event','2nd event','3rd event'],
        nav : [ 
            {Link: 'Services', Text: 'Services'},
            {Link: 'Portfolio', Text: 'Portfolio'},
            {Link: 'About', Text : 'About'},
            {Link: 'Team', Text : 'Team'},
            {Link: 'Contact', Text : 'Contact'},
            {Link: 'Events', Text : 'Events'}
        ]
    })
});

eventRouter.route('/event')
    .get(function(req,res){
    res.send('Hello Singel Event!');
});

module.exports = eventRouter;

